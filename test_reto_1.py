import sys
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.by import By


class TestReto1(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()

    def tearDown(self) -> None:
        self.driver.close()

    def test_reto1(self):
        driver = self.driver
        driver.get("http://www.rpachallenge.com/")
        # codigo here!
        #Button start
        button_start = driver.find_element_by_xpath("//button[contains(text(),'Start')]");
        button_start.click()
        #Set input elements:
        veces = 10
        first_name=["John","Jane","Albert","Michael","Doug","Jessie","Stan","Michelle","Stacy","Lara"]
        phone_number =["40716543298","40791345621","40735416854","40733652145","40799885412","40733154268","40712462257","40731254562","40741785214","40731653845"]
        role_company =["Analyst","Medical Engineer","Accountant","IT Specialist","Analyst","Scientist","Advisor","Scientist","HR Manager","Programmer"]
        email = ["jsmith@itsolutions.co.uk","jdorsey@mc.com","kipling@waterfront.com","mrobertson@mc.com","dderrick@timepath.co.uk","jmarlowe@aperture.us","shamm@sugarwell.org","mnorton@aperture.us","sshelby@techdev.com","lpalmer@timepath.co.uk"]
        company_name =["IT Solutions","MediCare","Waterfront","MediCare","Timepath Inc.","Aperture Inc.","Sugarwell","Aperture Inc.","TechDev","Timepath Inc."]
        last_name = ["Smith","Dorsey","Kipling","Robertson","Derrick","Marlowe","Hamm","Norton","Shelby","Palmer"]
        address = ["98 North Road","11 Crown Street","22 Guild Street","17 Farburn Terrace","99 Shire Oak Road","27 Cheshire Street","10 Dam Road","13 White Rabbit Street","19 Pineapple Boulevard","87 Orange Street"]

        for vez in range(veces):
            input_first_name = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelFirstName"]')
            input_first_name.send_keys(first_name[vez])

            input_phone_number = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelPhone"]')
            input_phone_number.send_keys(phone_number[vez])

            input_role_company = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelRole"]')
            input_role_company.send_keys(role_company[vez])

            input_email = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelEmail"]')
            input_email.send_keys(email[vez])

            input_company_name = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelCompanyName"]')
            input_company_name.send_keys(company_name[vez])

            input_last_name = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelLastName"]')
            input_last_name.send_keys(last_name[vez])

            #Adress
            input_address = self.driver.find_element_by_xpath('//input[@ng-reflect-name="labelAddress"]')
            input_address.send_keys(address[vez])

            button_submit =self.driver.find_element_by_xpath("//body/app-root[1]/div[2]/app-rpa1[1]/div[1]/div[2]/form[1]/input[1]")
            button_submit.click()

        #get score
        score_text =self.driver.find_element_by_class_name("message2")
        percentage = float(((score_text.text.split('%')[0].split('is')))[1])

        time.sleep(2)
        #Revisar que ese valor sea mayor a 90
        self.assertGreater(percentage, 90)


if __name__ == '__main__':
    unittest.main()
