import sys
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By


class TestRetoFinalCaso(unittest.TestCase):
    def setUp(self) -> None:
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.driver.set_window_size(1000, 2000)#Se agregó porque la pantalla no mostraba todo el contenido y no encontraba ciertos elementos. Al disminuir el ancho se reajusta el estilo mostrando todo los elementos requeridos


    def tearDown(self) -> None:
        self.driver.close()

    def test_reto_final_caso_arbitrario1(self):
        '''
        1.Clic Elements -->
        2. Clic Checkbox -->
        3. Clic "+" checkbox
        4. clic Home/Documents/Office/Public and Home/Downloads/WordFile
        4. Check if "Public" and "WordFile" are in message
         '''
        driver = self.driver
        driver.get("https://demoqa.com/automation-practice-form")

        div_show_elements = self.driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[2]")
        div_show_elements.click()

        div_show_check_box = self.driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[2]")
        div_show_check_box.click()

        span_more_checkbox =self.driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/button[1]")
        span_more_checkbox.click()

        span_public = self.driver.find_element_by_xpath("//span[contains(text(),'Public')]")
        span_public.click()

        span_wordfile = self.driver.find_element_by_xpath("//span[contains(text(),'Word File.doc')]")
        span_wordfile.click()
        #Get Result
        div_result = self.driver.find_element_by_xpath("//div[@id='result']")

        time.sleep(1)
        self.assertRegex(div_result.text, 'public')
        self.assertRegex(div_result.text, 'wordFile')


    def test_reto_final_caso_arbitrario2(self):
        '''
        1.Clic Elements -->
        2.clic Radio Button
        3.clic Impressive label checkbox to activate
        4.Check if selected was "Impressive"
        '''
        driver = self.driver
        driver.get("https://demoqa.com/automation-practice-form")

        div_show_elements = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[2]")
        div_show_elements.click()

        div_show_radio_button = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[3]")
        div_show_radio_button.click()

        label_impressive = driver.find_element_by_xpath("//label[contains(text(),'Impressive')]")
        label_impressive.click()

        p_result = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[2]/div[1]/p[1]")
        text_span_selected = p_result.find_element_by_css_selector("span")

        self.assertEqual('Impressive', text_span_selected.text)


    def test_reto_final_caso_arbitrario3(self):
        '''
        1. clic Interactions
        2. clic Droppable
        3. clic Accept
        4. drag "No Acceptable" and drop in Drop here
        5. check if message still "Drop here"
        6. drag "Acceptable" and drop in Drop here
        7. check if message still "Dropped!"
        '''
        driver = self.driver
        driver.get("https://demoqa.com/automation-practice-form")

        div_show_interactions = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[5]/span[1]/div[1]/div[2]")
        div_show_interactions.click()
        time.sleep(3)

        li_droppable = driver.find_element_by_xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[5]/div[1]/ul[1]/li[4]")
        li_droppable.click()

        a_accept = driver.find_element_by_xpath("//a[@id='droppableExample-tab-accept']")
        a_accept.click()

        div_source_notacceptable = driver.find_element_by_xpath("//div[@id='notAcceptable']")
        div_source_acceptable = driver.find_element_by_xpath("//div[@id='acceptable']")
        div_target_drophere = driver.find_element_by_xpath("//div[@id='acceptDropContainer']//div[@id='droppable']")

        action = ActionChains(driver)

        #Drag and drop "Not acceptable"
        action.drag_and_drop(div_source_notacceptable, div_target_drophere).perform()
        time.sleep(1)
        self.assertEqual(div_target_drophere.text, "Drop here")

        # Drag and drop "Acceptable"
        action.drag_and_drop(div_source_acceptable, div_target_drophere).perform()
        time.sleep(1)
        self.assertEqual(div_target_drophere.text, "Dropped!")


    def test_reto_final_caso_arbitrario4(self):
        '''
        1.clic Alerts, Frame & Windows
        2.clic Browser Windows
        3.clic New Tab
        4.check in new tab, the message "This is a sample page"
        '''
        driver = self.driver
        driver.get("https://demoqa.com/automation-practice-form")

        div_show_alerts_frame_windows =driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/span[1]/div[1]/div[2]")
        div_show_alerts_frame_windows.click()
        time.sleep(1)
        div_browser_windows = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[3]/div[1]/ul[1]/li[1]")
        div_browser_windows.click()
        time.sleep(1)
        button_new_tab = driver.find_element_by_id("tabButton")
        button_new_tab.click()
        time.sleep(1)
        #Go to new tab
        driver.switch_to.window(driver.window_handles[1])
        body = driver.find_element_by_xpath("//body")

        time.sleep(2)
        self.assertEqual(body.text, "This is a sample page")


    def test_reto_final_caso_arbitrario5(self):
        '''
        1.Clic Elements -->
        2.clic Buttons
        3. Right click en "Right Click Me"
        4.Check if was clicked with  right click in "Right Click Me" in message
        '''
        driver = self.driver
        driver.get("https://demoqa.com/automation-practice-form")

        div_show_elements = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/span[1]/div[1]/div[2]")
        div_show_elements.click()

        div_show_buttons = driver.find_element_by_xpath("//body/div[@id='app']/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/ul[1]/li[5]")
        div_show_buttons.click()

        button_rigth_click = driver.find_element_by_id("rightClickBtn")

        action_chain = ActionChains(driver)
        action_chain.context_click(button_rigth_click).perform()
        time.sleep(2)

        p_message = driver.find_element_by_id("rightClickMessage")

        self.assertRegex(p_message.text,'done a right click')


if __name__ == '__main__':
    unittest.main()
